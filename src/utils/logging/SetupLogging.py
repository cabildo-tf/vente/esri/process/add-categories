import os
import yaml
import logging.config


class SetupLogging:
    def __init__(self, path):
        self.default_config = os.path.join(path)

    def setup_logging(self, default_level=logging.INFO, env_key='LOG_CFG'):
        path = os.getenv(env_key, self.default_config)
        if os.path.exists(path):
            with open(path, 'r') as f:
                config = yaml.safe_load(f.read())
                logging.config.dictConfig(config)
        else:
            print('Error in loading logging configuration. Using default configs')
            logging.basicConfig(level=default_level)
