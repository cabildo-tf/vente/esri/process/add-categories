import logging
from arcgis.gis import GIS, ContentManager

from os import path, environ
import yaml
from utils.logging.SetupLogging import SetupLogging
from itertools import islice


def load_config(config_path):
    if path.exists(config_path):
        with open(config_path, 'r') as f:
            return yaml.safe_load(f.read())


def connect(username, password, server_base_url):
    logging.info("Connecting to %s", server_base_url)
    server = GIS(url=f"{server_base_url}", username=username, password=password, initialize=True, verify_cert=False)
    logging.info("Connected")
    return server


def chunks(data, size=100):
    it = iter(data)
    for i in range(0, len(data), size):
        yield {k: data[k] for k in islice(it, size)}


def main():
    username = environ.get('USERNAME', 'admin')
    password = environ.get('PASSWORD', 'changeme')
    server_base_url = environ.get('SERVER_URL', 'https://arcgisonline.com')
    config_path = environ.get('CONFIG_PATH', './config.yaml')
    config_log_path = environ.get('CONFIG_LOG_PATH', './config.yaml')

    SetupLogging(config_log_path).setup_logging()

    config = load_config(config_path)
    gis_server = connect(username, password, server_base_url)
    cm = ContentManager(gis_server)

    services_group = chunks(config['services'])
    for services_pack in services_group:
        updates = []
        for key, value in services_pack.items():
            items = cm.search(query='title:"{0}"'.format(key))
            if len(items) == 1:
                item = items[0]
                updates.append({item.id: value})
            elif len(items) == 0:
                logging.warning("El servicio no existe '{0}'".format(key))
            else:
                logging.warning("Existe más de un servicio con el nombre '{0}'".format(key))
        cm.categories.assign_to_items(updates)


if __name__ == '__main__':
    main()
