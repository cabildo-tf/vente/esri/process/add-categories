# Add Categories
Script que permite asignar categorías a los servicios existentes en Portal for ArcGIS. 

Para la asignación de las categorías se usa un fichero en formato YAML, donde <nombre_servicios>
es nombre del servicio y <categoría_1>, <categoria_2>, ... <categoría_N> es un listado con las categorías
a asignar a ese servicio.

```yaml
services:
  <nombre_servicio>:
    categories:
      - <categoría_1>
      - <categoría_2>
      - ...
      - <categoría_N>
```

La conexión a Portal for ArcGIS se realiza a través de variables de entorno:

| Variable        | Descripción                                            | Valor por defecto        |
|-----------------|--------------------------------------------------------|--------------------------|
| USERNAME        | Usuario en el Portal con capacidad de edición          | admin                    |
| PASSWORD        | Contraseña del usuario                                 | changeme                 |
| SERVER_URL      | Url al Portal                                          | https://arcgisonline.com |
| CONFIG_PATH     | Ruta al fichero con la configuración de las categorías | ./config.yaml            |
| CONFIG_LOG_PATH | Ruta al fichro con la configuración al log             | ./config.yaml            |

# TODO
Falta crear un docker para la automatización